$(function() {
	var serverUrl = "server.php";

	/*Пример класа для работы с сервером*/
	var Api = (function( _url ){

		var _self = {
			url: "",
			init: function( url )
			{
				this.url = url;
			},
			methods: []
		};

		/*****Методы*****/
			_self.methods.test1 = function( text, callback /*Функция коллбек, вызываем её в complete*/ )
			{
				$.ajax({
					url: _self.url, //Путь до скрипта сервера
					method: "GET", 	//Метод отправки данных, GET - это передача данных через строку адреса, в данном случае будет что типа: server.php?method=test1&text=%значение переменной tetx%
					data: {			/*То что сервер получит от браузера*/
						method: "test1",
						text: text
					},
					/*Эта функция выполнится когда сервер ответит*/
					complete: function( response /*- тут ответ сервера в формате string*/ )
					{
						console.log(response ); //Объект с инфой о запросе, статус и тд

						callback( response.responseText /* Текст который вернул сервер */ );
					}
				});
			};

			_self.methods.test2 = function( text, callback /*Функция коллбек, вызываем её в complete*/ )
			{
				$.ajax({
					url: _self.url, //Путь до скрипта сервера
					method: "GET", 	//Метод отправки данных, GET - это передача данных через строку адреса, в данном случае будет что типа: server.php?method=test1&text=%значение переменной tetx%
					data: {			/*То что сервер получит от браузера*/
						method: "test2",
						text: text
					},
					/*Эта функция выполнится когда сервер ответит*/
					complete: function( response /*- тут ответ сервера в формате string*/ )
					{
						console.log(response ); //Объект с инфой о запросе, статус и тд

						callback( response.responseText /* Текст который вернул сервер */ );
					}
				});
			};
		/****************/

		_self.init( _url );

		return _self;
	});

	var exampleServer = Api( serverUrl );

	/*Находим кнопку с data атрибутом data-method="test1"*/
		$( '[data-method="test1"]' ).on( 'click', function(){
			exampleServer.methods.test1( "Юзер нажал на кнопку лол", function /*Функция коллбека которая вызывается в complete*/( response ){
				alert( "Сервер ответил: " + response );
			});
		});

		$( '[data-method="test2"]' ).on( 'click', function(){
			exampleServer.methods.test2( "Юзер нажал на кнопку лол 2", function /*Функция коллбека которая вызывается в complete*/( response ){
				alert( "Сервер ответил: " + response );
			});
		});
});